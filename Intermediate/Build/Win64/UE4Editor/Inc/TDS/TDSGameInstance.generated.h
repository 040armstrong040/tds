// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
struct FWeaponInfo;
#ifdef TDS_TDSGameInstance_generated_h
#error "TDSGameInstance.generated.h already included, missing '#pragma once' in TDSGameInstance.h"
#endif
#define TDS_TDSGameInstance_generated_h

#define TDS_Source_TDS_TDSGameInstance_h_19_SPARSE_DATA
#define TDS_Source_TDS_TDSGameInstance_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWeaponInfoByName);


#define TDS_Source_TDS_TDSGameInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWeaponInfoByName);


#define TDS_Source_TDS_TDSGameInstance_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUTDSGameInstance(); \
	friend struct Z_Construct_UClass_UTDSGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTDSGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UTDSGameInstance)


#define TDS_Source_TDS_TDSGameInstance_h_19_INCLASS \
private: \
	static void StaticRegisterNativesUTDSGameInstance(); \
	friend struct Z_Construct_UClass_UTDSGameInstance_Statics; \
public: \
	DECLARE_CLASS(UTDSGameInstance, UGameInstance, COMPILED_IN_FLAGS(0 | CLASS_Transient), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(UTDSGameInstance)


#define TDS_Source_TDS_TDSGameInstance_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTDSGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTDSGameInstance) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTDSGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTDSGameInstance); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTDSGameInstance(UTDSGameInstance&&); \
	NO_API UTDSGameInstance(const UTDSGameInstance&); \
public:


#define TDS_Source_TDS_TDSGameInstance_h_19_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UTDSGameInstance(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UTDSGameInstance(UTDSGameInstance&&); \
	NO_API UTDSGameInstance(const UTDSGameInstance&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UTDSGameInstance); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UTDSGameInstance); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UTDSGameInstance)


#define TDS_Source_TDS_TDSGameInstance_h_19_PRIVATE_PROPERTY_OFFSET
#define TDS_Source_TDS_TDSGameInstance_h_16_PROLOG
#define TDS_Source_TDS_TDSGameInstance_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_TDSGameInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_TDSGameInstance_h_19_SPARSE_DATA \
	TDS_Source_TDS_TDSGameInstance_h_19_RPC_WRAPPERS \
	TDS_Source_TDS_TDSGameInstance_h_19_INCLASS \
	TDS_Source_TDS_TDSGameInstance_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_Source_TDS_TDSGameInstance_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_TDSGameInstance_h_19_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_TDSGameInstance_h_19_SPARSE_DATA \
	TDS_Source_TDS_TDSGameInstance_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_Source_TDS_TDSGameInstance_h_19_INCLASS_NO_PURE_DECLS \
	TDS_Source_TDS_TDSGameInstance_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class UTDSGameInstance>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_Source_TDS_TDSGameInstance_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
