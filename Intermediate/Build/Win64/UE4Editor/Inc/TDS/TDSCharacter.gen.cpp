// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TDS/Character/TDSCharacter.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDSCharacter() {}
// Cross Module References
	TDS_API UClass* Z_Construct_UClass_ATDSCharacter_NoRegister();
	TDS_API UClass* Z_Construct_UClass_ATDSCharacter();
	ENGINE_API UClass* Z_Construct_UClass_ACharacter();
	UPackage* Z_Construct_UPackage__Script_TDS();
	TDS_API UClass* Z_Construct_UClass_AWeaponDefault_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UDecalComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UAnimMontage_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UCameraComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_USpringArmComponent_NoRegister();
	ENGINE_API UClass* Z_Construct_UClass_UMaterialInterface_NoRegister();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	TDS_API UEnum* Z_Construct_UEnum_TDS_EMovementState();
	TDS_API UScriptStruct* Z_Construct_UScriptStruct_FCharacterSpeed();
// End Cross Module References
	DEFINE_FUNCTION(ATDSCharacter::execWeaponReloadEnd_BP)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WeaponReloadEnd_BP_Implementation();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execWeaponReloadStart_BP)
	{
		P_GET_OBJECT(UAnimMontage,Z_Param_Anim);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WeaponReloadStart_BP_Implementation(Z_Param_Anim);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execWeaponReloadEnd)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WeaponReloadEnd();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execWeaponReloadStart)
	{
		P_GET_OBJECT(UAnimMontage,Z_Param_Anim);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->WeaponReloadStart(Z_Param_Anim);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execTryReloadWeapon)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->TryReloadWeapon();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execAttackCharEvent)
	{
		P_GET_UBOOL(Z_Param_bIsFiring);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->AttackCharEvent(Z_Param_bIsFiring);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execGetCurrentWeapon)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(AWeaponDefault**)Z_Param__Result=P_THIS->GetCurrentWeapon();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execInitWeapon)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_IdWeapon);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InitWeapon(Z_Param_IdWeapon);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execGetCursorToWorld)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		*(UDecalComponent**)Z_Param__Result=P_THIS->GetCursorToWorld();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execChangeMovementState)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->ChangeMovementState();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execCharacterUpdate)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->CharacterUpdate();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execMovementTick)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_DeltaTime);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->MovementTick(Z_Param_DeltaTime);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execInputAttackReleased)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InputAttackReleased();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execInputAttackPressed)
	{
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InputAttackPressed();
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execInputAxisY)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InputAxisY(Z_Param_Value);
		P_NATIVE_END;
	}
	DEFINE_FUNCTION(ATDSCharacter::execInputAxisX)
	{
		P_GET_PROPERTY(FFloatProperty,Z_Param_Value);
		P_FINISH;
		P_NATIVE_BEGIN;
		P_THIS->InputAxisX(Z_Param_Value);
		P_NATIVE_END;
	}
	static FName NAME_ATDSCharacter_WeaponReloadEnd_BP = FName(TEXT("WeaponReloadEnd_BP"));
	void ATDSCharacter::WeaponReloadEnd_BP()
	{
		ProcessEvent(FindFunctionChecked(NAME_ATDSCharacter_WeaponReloadEnd_BP),NULL);
	}
	static FName NAME_ATDSCharacter_WeaponReloadStart_BP = FName(TEXT("WeaponReloadStart_BP"));
	void ATDSCharacter::WeaponReloadStart_BP(UAnimMontage* Anim)
	{
		TDSCharacter_eventWeaponReloadStart_BP_Parms Parms;
		Parms.Anim=Anim;
		ProcessEvent(FindFunctionChecked(NAME_ATDSCharacter_WeaponReloadStart_BP),&Parms);
	}
	void ATDSCharacter::StaticRegisterNativesATDSCharacter()
	{
		UClass* Class = ATDSCharacter::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "AttackCharEvent", &ATDSCharacter::execAttackCharEvent },
			{ "ChangeMovementState", &ATDSCharacter::execChangeMovementState },
			{ "CharacterUpdate", &ATDSCharacter::execCharacterUpdate },
			{ "GetCurrentWeapon", &ATDSCharacter::execGetCurrentWeapon },
			{ "GetCursorToWorld", &ATDSCharacter::execGetCursorToWorld },
			{ "InitWeapon", &ATDSCharacter::execInitWeapon },
			{ "InputAttackPressed", &ATDSCharacter::execInputAttackPressed },
			{ "InputAttackReleased", &ATDSCharacter::execInputAttackReleased },
			{ "InputAxisX", &ATDSCharacter::execInputAxisX },
			{ "InputAxisY", &ATDSCharacter::execInputAxisY },
			{ "MovementTick", &ATDSCharacter::execMovementTick },
			{ "TryReloadWeapon", &ATDSCharacter::execTryReloadWeapon },
			{ "WeaponReloadEnd", &ATDSCharacter::execWeaponReloadEnd },
			{ "WeaponReloadEnd_BP", &ATDSCharacter::execWeaponReloadEnd_BP },
			{ "WeaponReloadStart", &ATDSCharacter::execWeaponReloadStart },
			{ "WeaponReloadStart_BP", &ATDSCharacter::execWeaponReloadStart_BP },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics
	{
		struct TDSCharacter_eventAttackCharEvent_Parms
		{
			bool bIsFiring;
		};
		static void NewProp_bIsFiring_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_bIsFiring;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	void Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::NewProp_bIsFiring_SetBit(void* Obj)
	{
		((TDSCharacter_eventAttackCharEvent_Parms*)Obj)->bIsFiring = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::NewProp_bIsFiring = { "bIsFiring", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TDSCharacter_eventAttackCharEvent_Parms), &Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::NewProp_bIsFiring_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::NewProp_bIsFiring,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "AttackCharEvent", nullptr, nullptr, sizeof(TDSCharacter_eventAttackCharEvent_Parms), Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_AttackCharEvent()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_AttackCharEvent_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_ChangeMovementState_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_ChangeMovementState_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_ChangeMovementState_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "ChangeMovementState", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_ChangeMovementState_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_ChangeMovementState_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_ChangeMovementState()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_ChangeMovementState_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_CharacterUpdate_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_CharacterUpdate_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_CharacterUpdate_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "CharacterUpdate", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_CharacterUpdate_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_CharacterUpdate_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_CharacterUpdate()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_CharacterUpdate_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics
	{
		struct TDSCharacter_eventGetCurrentWeapon_Parms
		{
			AWeaponDefault* ReturnValue;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventGetCurrentWeapon_Parms, ReturnValue), Z_Construct_UClass_AWeaponDefault_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "GetCurrentWeapon", nullptr, nullptr, sizeof(TDSCharacter_eventGetCurrentWeapon_Parms), Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics
	{
		struct TDSCharacter_eventGetCursorToWorld_Parms
		{
			UDecalComponent* ReturnValue;
		};
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ReturnValue_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::NewProp_ReturnValue_MetaData[] = {
		{ "EditInline", "true" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000080588, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventGetCursorToWorld_Parms, ReturnValue), Z_Construct_UClass_UDecalComponent_NoRegister, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::NewProp_ReturnValue_MetaData, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::NewProp_ReturnValue_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "GetCursorToWorld", nullptr, nullptr, sizeof(TDSCharacter_eventGetCursorToWorld_Parms), Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics
	{
		struct TDSCharacter_eventInitWeapon_Parms
		{
			FName IdWeapon;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_IdWeapon;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::NewProp_IdWeapon = { "IdWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventInitWeapon_Parms, IdWeapon), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::NewProp_IdWeapon,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "InitWeapon", nullptr, nullptr, sizeof(TDSCharacter_eventInitWeapon_Parms), Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_InitWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_InitWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_InputAttackPressed_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_InputAttackPressed_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_InputAttackPressed_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "InputAttackPressed", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_InputAttackPressed_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InputAttackPressed_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_InputAttackPressed()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_InputAttackPressed_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_InputAttackReleased_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_InputAttackReleased_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_InputAttackReleased_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "InputAttackReleased", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_InputAttackReleased_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InputAttackReleased_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_InputAttackReleased()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_InputAttackReleased_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics
	{
		struct TDSCharacter_eventInputAxisX_Parms
		{
			float Value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventInputAxisX_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "InputAxisX", nullptr, nullptr, sizeof(TDSCharacter_eventInputAxisX_Parms), Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_InputAxisX()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_InputAxisX_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics
	{
		struct TDSCharacter_eventInputAxisY_Parms
		{
			float Value;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_Value;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::NewProp_Value = { "Value", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventInputAxisY_Parms, Value), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::NewProp_Value,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "InputAxisY", nullptr, nullptr, sizeof(TDSCharacter_eventInputAxisY_Parms), Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_InputAxisY()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_InputAxisY_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics
	{
		struct TDSCharacter_eventMovementTick_Parms
		{
			float DeltaTime;
		};
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_DeltaTime;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::NewProp_DeltaTime = { "DeltaTime", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventMovementTick_Parms, DeltaTime), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::NewProp_DeltaTime,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "MovementTick", nullptr, nullptr, sizeof(TDSCharacter_eventMovementTick_Parms), Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_MovementTick()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_MovementTick_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "TryReloadWeapon", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "WeaponReloadEnd", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP_Statics
	{
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "WeaponReloadEnd_BP", nullptr, nullptr, 0, nullptr, 0, RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics
	{
		struct TDSCharacter_eventWeaponReloadStart_Parms
		{
			UAnimMontage* Anim;
		};
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Anim;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::NewProp_Anim = { "Anim", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventWeaponReloadStart_Parms, Anim), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::NewProp_Anim,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "WeaponReloadStart", nullptr, nullptr, sizeof(TDSCharacter_eventWeaponReloadStart_Parms), Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x00020401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	struct Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics
	{
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_Anim;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::NewProp_Anim = { "Anim", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSCharacter_eventWeaponReloadStart_BP_Parms, Anim), Z_Construct_UClass_UAnimMontage_NoRegister, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::NewProp_Anim,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_ATDSCharacter, nullptr, "WeaponReloadStart_BP", nullptr, nullptr, sizeof(TDSCharacter_eventWeaponReloadStart_BP_Parms), Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x08020C00, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_ATDSCharacter_NoRegister()
	{
		return ATDSCharacter::StaticClass();
	}
	struct Z_Construct_UClass_ATDSCharacter_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_TopDownCameraComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_TopDownCameraComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CameraBoom_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CameraBoom;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_InitWeaponName_MetaData[];
#endif
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_InitWeaponName;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CursorMaterial_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_CursorMaterial;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_CursorSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_CursorSize;
		static const UE4CodeGen_Private::FBytePropertyParams NewProp_MovementState_Underlying;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementState_MetaData[];
#endif
		static const UE4CodeGen_Private::FEnumPropertyParams NewProp_MovementState;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MovementSpeedInfo_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_MovementSpeedInfo;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_SprintRunEnabled_MetaData[];
#endif
		static void NewProp_SprintRunEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_SprintRunEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WalkEnabled_MetaData[];
#endif
		static void NewProp_WalkEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_WalkEnabled;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_AimEnabled_MetaData[];
#endif
		static void NewProp_AimEnabled_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_AimEnabled;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ATDSCharacter_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_ACharacter,
		(UObject* (*)())Z_Construct_UPackage__Script_TDS,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_ATDSCharacter_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_ATDSCharacter_AttackCharEvent, "AttackCharEvent" }, // 1832873423
		{ &Z_Construct_UFunction_ATDSCharacter_ChangeMovementState, "ChangeMovementState" }, // 3576576136
		{ &Z_Construct_UFunction_ATDSCharacter_CharacterUpdate, "CharacterUpdate" }, // 1988608546
		{ &Z_Construct_UFunction_ATDSCharacter_GetCurrentWeapon, "GetCurrentWeapon" }, // 3635542605
		{ &Z_Construct_UFunction_ATDSCharacter_GetCursorToWorld, "GetCursorToWorld" }, // 1380763193
		{ &Z_Construct_UFunction_ATDSCharacter_InitWeapon, "InitWeapon" }, // 1758049685
		{ &Z_Construct_UFunction_ATDSCharacter_InputAttackPressed, "InputAttackPressed" }, // 1415024014
		{ &Z_Construct_UFunction_ATDSCharacter_InputAttackReleased, "InputAttackReleased" }, // 1922905317
		{ &Z_Construct_UFunction_ATDSCharacter_InputAxisX, "InputAxisX" }, // 1297986021
		{ &Z_Construct_UFunction_ATDSCharacter_InputAxisY, "InputAxisY" }, // 3496637930
		{ &Z_Construct_UFunction_ATDSCharacter_MovementTick, "MovementTick" }, // 2130388356
		{ &Z_Construct_UFunction_ATDSCharacter_TryReloadWeapon, "TryReloadWeapon" }, // 1386900153
		{ &Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd, "WeaponReloadEnd" }, // 888017244
		{ &Z_Construct_UFunction_ATDSCharacter_WeaponReloadEnd_BP, "WeaponReloadEnd_BP" }, // 1601687132
		{ &Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart, "WeaponReloadStart" }, // 3946944876
		{ &Z_Construct_UFunction_ATDSCharacter_WeaponReloadStart_BP, "WeaponReloadStart_BP" }, // 2662973200
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::Class_MetaDataParams[] = {
		{ "BlueprintType", "true" },
		{ "HideCategories", "Navigation" },
		{ "IncludePath", "Character/TDSCharacter.h" },
		{ "IsBlueprintBase", "true" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_TopDownCameraComponent_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Top down camera */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
		{ "ToolTip", "Top down camera" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_TopDownCameraComponent = { "TopDownCameraComponent", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, TopDownCameraComponent), Z_Construct_UClass_UCameraComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_TopDownCameraComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_TopDownCameraComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CameraBoom_MetaData[] = {
		{ "AllowPrivateAccess", "true" },
		{ "Category", "Camera" },
		{ "Comment", "/** Camera boom positioning the camera above the character */" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
		{ "ToolTip", "Camera boom positioning the camera above the character" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CameraBoom = { "CameraBoom", nullptr, (EPropertyFlags)0x00400000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, CameraBoom), Z_Construct_UClass_USpringArmComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CameraBoom_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CameraBoom_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_InitWeaponName_MetaData[] = {
		{ "Category", "Demo" },
		{ "Comment", "//Weapon\x09\n" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
		{ "ToolTip", "Weapon" },
	};
#endif
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_InitWeaponName = { "InitWeaponName", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, InitWeaponName), METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_InitWeaponName_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_InitWeaponName_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorMaterial_MetaData[] = {
		{ "Category", "Cursor" },
		{ "Comment", "//Cursor\n" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
		{ "ToolTip", "Cursor" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorMaterial = { "CursorMaterial", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, CursorMaterial), Z_Construct_UClass_UMaterialInterface_NoRegister, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorMaterial_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorMaterial_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorSize_MetaData[] = {
		{ "Category", "Cursor" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorSize = { "CursorSize", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, CursorSize), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorSize_MetaData)) };
	const UE4CodeGen_Private::FBytePropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState_Underlying = { "UnderlyingType", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Byte, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, nullptr, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FEnumPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState = { "MovementState", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Enum, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, MovementState), Z_Construct_UEnum_TDS_EMovementState, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementSpeedInfo_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementSpeedInfo = { "MovementSpeedInfo", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ATDSCharacter, MovementSpeedInfo), Z_Construct_UScriptStruct_FCharacterSpeed, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementSpeedInfo_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementSpeedInfo_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled_SetBit(void* Obj)
	{
		((ATDSCharacter*)Obj)->SprintRunEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled = { "SprintRunEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ATDSCharacter), &Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled_SetBit(void* Obj)
	{
		((ATDSCharacter*)Obj)->WalkEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled = { "WalkEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ATDSCharacter), &Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled_MetaData[] = {
		{ "Category", "Movement" },
		{ "ModuleRelativePath", "Character/TDSCharacter.h" },
	};
#endif
	void Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled_SetBit(void* Obj)
	{
		((ATDSCharacter*)Obj)->AimEnabled = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled = { "AimEnabled", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(ATDSCharacter), &Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled_SetBit, METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ATDSCharacter_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_TopDownCameraComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CameraBoom,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_InitWeaponName,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorMaterial,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_CursorSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState_Underlying,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementState,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_MovementSpeedInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_SprintRunEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_WalkEnabled,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ATDSCharacter_Statics::NewProp_AimEnabled,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ATDSCharacter_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ATDSCharacter>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ATDSCharacter_Statics::ClassParams = {
		&ATDSCharacter::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_ATDSCharacter_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::PropPointers),
		0,
		0x008000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ATDSCharacter_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ATDSCharacter_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ATDSCharacter()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ATDSCharacter_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ATDSCharacter, 2381840563);
	template<> TDS_API UClass* StaticClass<ATDSCharacter>()
	{
		return ATDSCharacter::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ATDSCharacter(Z_Construct_UClass_ATDSCharacter, &ATDSCharacter::StaticClass, TEXT("/Script/TDS"), TEXT("ATDSCharacter"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ATDSCharacter);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
