// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UAnimMontage;
class AWeaponDefault;
class UDecalComponent;
#ifdef TDS_TDSCharacter_generated_h
#error "TDSCharacter.generated.h already included, missing '#pragma once' in TDSCharacter.h"
#endif
#define TDS_TDSCharacter_generated_h

#define TDS_Source_TDS_Character_TDSCharacter_h_15_SPARSE_DATA
#define TDS_Source_TDS_Character_TDSCharacter_h_15_RPC_WRAPPERS \
	virtual void WeaponReloadEnd_BP_Implementation(); \
	virtual void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim); \
 \
	DECLARE_FUNCTION(execWeaponReloadEnd_BP); \
	DECLARE_FUNCTION(execWeaponReloadStart_BP); \
	DECLARE_FUNCTION(execWeaponReloadEnd); \
	DECLARE_FUNCTION(execWeaponReloadStart); \
	DECLARE_FUNCTION(execTryReloadWeapon); \
	DECLARE_FUNCTION(execAttackCharEvent); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execInitWeapon); \
	DECLARE_FUNCTION(execGetCursorToWorld); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAttackReleased); \
	DECLARE_FUNCTION(execInputAttackPressed); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX);


#define TDS_Source_TDS_Character_TDSCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	virtual void WeaponReloadEnd_BP_Implementation(); \
	virtual void WeaponReloadStart_BP_Implementation(UAnimMontage* Anim); \
 \
	DECLARE_FUNCTION(execWeaponReloadEnd_BP); \
	DECLARE_FUNCTION(execWeaponReloadStart_BP); \
	DECLARE_FUNCTION(execWeaponReloadEnd); \
	DECLARE_FUNCTION(execWeaponReloadStart); \
	DECLARE_FUNCTION(execTryReloadWeapon); \
	DECLARE_FUNCTION(execAttackCharEvent); \
	DECLARE_FUNCTION(execGetCurrentWeapon); \
	DECLARE_FUNCTION(execInitWeapon); \
	DECLARE_FUNCTION(execGetCursorToWorld); \
	DECLARE_FUNCTION(execChangeMovementState); \
	DECLARE_FUNCTION(execCharacterUpdate); \
	DECLARE_FUNCTION(execMovementTick); \
	DECLARE_FUNCTION(execInputAttackReleased); \
	DECLARE_FUNCTION(execInputAttackPressed); \
	DECLARE_FUNCTION(execInputAxisY); \
	DECLARE_FUNCTION(execInputAxisX);


#define TDS_Source_TDS_Character_TDSCharacter_h_15_EVENT_PARMS \
	struct TDSCharacter_eventWeaponReloadStart_BP_Parms \
	{ \
		UAnimMontage* Anim; \
	};


#define TDS_Source_TDS_Character_TDSCharacter_h_15_CALLBACK_WRAPPERS
#define TDS_Source_TDS_Character_TDSCharacter_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesATDSCharacter(); \
	friend struct Z_Construct_UClass_ATDSCharacter_Statics; \
public: \
	DECLARE_CLASS(ATDSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(ATDSCharacter)


#define TDS_Source_TDS_Character_TDSCharacter_h_15_INCLASS \
private: \
	static void StaticRegisterNativesATDSCharacter(); \
	friend struct Z_Construct_UClass_ATDSCharacter_Statics; \
public: \
	DECLARE_CLASS(ATDSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(ATDSCharacter)


#define TDS_Source_TDS_Character_TDSCharacter_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ATDSCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ATDSCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDSCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDSCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDSCharacter(ATDSCharacter&&); \
	NO_API ATDSCharacter(const ATDSCharacter&); \
public:


#define TDS_Source_TDS_Character_TDSCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ATDSCharacter(ATDSCharacter&&); \
	NO_API ATDSCharacter(const ATDSCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ATDSCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ATDSCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ATDSCharacter)


#define TDS_Source_TDS_Character_TDSCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__TopDownCameraComponent() { return STRUCT_OFFSET(ATDSCharacter, TopDownCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ATDSCharacter, CameraBoom); }


#define TDS_Source_TDS_Character_TDSCharacter_h_12_PROLOG \
	TDS_Source_TDS_Character_TDSCharacter_h_15_EVENT_PARMS


#define TDS_Source_TDS_Character_TDSCharacter_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_Character_TDSCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_Character_TDSCharacter_h_15_SPARSE_DATA \
	TDS_Source_TDS_Character_TDSCharacter_h_15_RPC_WRAPPERS \
	TDS_Source_TDS_Character_TDSCharacter_h_15_CALLBACK_WRAPPERS \
	TDS_Source_TDS_Character_TDSCharacter_h_15_INCLASS \
	TDS_Source_TDS_Character_TDSCharacter_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_Source_TDS_Character_TDSCharacter_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_Character_TDSCharacter_h_15_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_Character_TDSCharacter_h_15_SPARSE_DATA \
	TDS_Source_TDS_Character_TDSCharacter_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_Source_TDS_Character_TDSCharacter_h_15_CALLBACK_WRAPPERS \
	TDS_Source_TDS_Character_TDSCharacter_h_15_INCLASS_NO_PURE_DECLS \
	TDS_Source_TDS_Character_TDSCharacter_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class ATDSCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_Source_TDS_Character_TDSCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
