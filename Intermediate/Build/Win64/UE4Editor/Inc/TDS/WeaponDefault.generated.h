// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UAnimMontage;
#ifdef TDS_WeaponDefault_generated_h
#error "WeaponDefault.generated.h already included, missing '#pragma once' in WeaponDefault.h"
#endif
#define TDS_WeaponDefault_generated_h

#define TDS_Source_TDS_WeaponDefault_h_14_DELEGATE \
static inline void FOnWeaponReloadEnd_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponReloadEnd) \
{ \
	OnWeaponReloadEnd.ProcessMulticastDelegate<UObject>(NULL); \
}


#define TDS_Source_TDS_WeaponDefault_h_13_DELEGATE \
struct _Script_TDS_eventOnWeaponReloadStart_Parms \
{ \
	UAnimMontage* Anim; \
}; \
static inline void FOnWeaponReloadStart_DelegateWrapper(const FMulticastScriptDelegate& OnWeaponReloadStart, UAnimMontage* Anim) \
{ \
	_Script_TDS_eventOnWeaponReloadStart_Parms Parms; \
	Parms.Anim=Anim; \
	OnWeaponReloadStart.ProcessMulticastDelegate<UObject>(&Parms); \
}


#define TDS_Source_TDS_WeaponDefault_h_19_SPARSE_DATA
#define TDS_Source_TDS_WeaponDefault_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetWeaponRound); \
	DECLARE_FUNCTION(execSetWeaponStateFire);


#define TDS_Source_TDS_WeaponDefault_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetWeaponRound); \
	DECLARE_FUNCTION(execSetWeaponStateFire);


#define TDS_Source_TDS_WeaponDefault_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponDefault(); \
	friend struct Z_Construct_UClass_AWeaponDefault_Statics; \
public: \
	DECLARE_CLASS(AWeaponDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(AWeaponDefault)


#define TDS_Source_TDS_WeaponDefault_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponDefault(); \
	friend struct Z_Construct_UClass_AWeaponDefault_Statics; \
public: \
	DECLARE_CLASS(AWeaponDefault, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/TDS"), NO_API) \
	DECLARE_SERIALIZER(AWeaponDefault)


#define TDS_Source_TDS_WeaponDefault_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponDefault(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponDefault) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponDefault); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponDefault(AWeaponDefault&&); \
	NO_API AWeaponDefault(const AWeaponDefault&); \
public:


#define TDS_Source_TDS_WeaponDefault_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponDefault(AWeaponDefault&&); \
	NO_API AWeaponDefault(const AWeaponDefault&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponDefault); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponDefault); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponDefault)


#define TDS_Source_TDS_WeaponDefault_h_19_PRIVATE_PROPERTY_OFFSET
#define TDS_Source_TDS_WeaponDefault_h_16_PROLOG
#define TDS_Source_TDS_WeaponDefault_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_WeaponDefault_h_19_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_WeaponDefault_h_19_SPARSE_DATA \
	TDS_Source_TDS_WeaponDefault_h_19_RPC_WRAPPERS \
	TDS_Source_TDS_WeaponDefault_h_19_INCLASS \
	TDS_Source_TDS_WeaponDefault_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define TDS_Source_TDS_WeaponDefault_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	TDS_Source_TDS_WeaponDefault_h_19_PRIVATE_PROPERTY_OFFSET \
	TDS_Source_TDS_WeaponDefault_h_19_SPARSE_DATA \
	TDS_Source_TDS_WeaponDefault_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	TDS_Source_TDS_WeaponDefault_h_19_INCLASS_NO_PURE_DECLS \
	TDS_Source_TDS_WeaponDefault_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> TDS_API UClass* StaticClass<class AWeaponDefault>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID TDS_Source_TDS_WeaponDefault_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
