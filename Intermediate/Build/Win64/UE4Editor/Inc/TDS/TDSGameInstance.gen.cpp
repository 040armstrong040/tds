// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "TDS/TDSGameInstance.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeTDSGameInstance() {}
// Cross Module References
	TDS_API UClass* Z_Construct_UClass_UTDSGameInstance_NoRegister();
	TDS_API UClass* Z_Construct_UClass_UTDSGameInstance();
	ENGINE_API UClass* Z_Construct_UClass_UGameInstance();
	UPackage* Z_Construct_UPackage__Script_TDS();
	TDS_API UScriptStruct* Z_Construct_UScriptStruct_FWeaponInfo();
	ENGINE_API UClass* Z_Construct_UClass_UDataTable_NoRegister();
// End Cross Module References
	DEFINE_FUNCTION(UTDSGameInstance::execGetWeaponInfoByName)
	{
		P_GET_PROPERTY(FNameProperty,Z_Param_NameWeapon);
		P_GET_STRUCT_REF(FWeaponInfo,Z_Param_Out_OutInfo);
		P_FINISH;
		P_NATIVE_BEGIN;
		*(bool*)Z_Param__Result=P_THIS->GetWeaponInfoByName(Z_Param_NameWeapon,Z_Param_Out_OutInfo);
		P_NATIVE_END;
	}
	void UTDSGameInstance::StaticRegisterNativesUTDSGameInstance()
	{
		UClass* Class = UTDSGameInstance::StaticClass();
		static const FNameNativePtrPair Funcs[] = {
			{ "GetWeaponInfoByName", &UTDSGameInstance::execGetWeaponInfoByName },
		};
		FNativeFunctionRegistrar::RegisterFunctions(Class, Funcs, UE_ARRAY_COUNT(Funcs));
	}
	struct Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics
	{
		struct TDSGameInstance_eventGetWeaponInfoByName_Parms
		{
			FName NameWeapon;
			FWeaponInfo OutInfo;
			bool ReturnValue;
		};
		static const UE4CodeGen_Private::FNamePropertyParams NewProp_NameWeapon;
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_OutInfo;
		static void NewProp_ReturnValue_SetBit(void* Obj);
		static const UE4CodeGen_Private::FBoolPropertyParams NewProp_ReturnValue;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Function_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FFunctionParams FuncParams;
	};
	const UE4CodeGen_Private::FNamePropertyParams Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_NameWeapon = { "NameWeapon", nullptr, (EPropertyFlags)0x0010000000000080, UE4CodeGen_Private::EPropertyGenFlags::Name, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSGameInstance_eventGetWeaponInfoByName_Parms, NameWeapon), METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_OutInfo = { "OutInfo", nullptr, (EPropertyFlags)0x0010008000000180, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(TDSGameInstance_eventGetWeaponInfoByName_Parms, OutInfo), Z_Construct_UScriptStruct_FWeaponInfo, METADATA_PARAMS(nullptr, 0) };
	void Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_ReturnValue_SetBit(void* Obj)
	{
		((TDSGameInstance_eventGetWeaponInfoByName_Parms*)Obj)->ReturnValue = 1;
	}
	const UE4CodeGen_Private::FBoolPropertyParams Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_ReturnValue = { "ReturnValue", nullptr, (EPropertyFlags)0x0010000000000580, UE4CodeGen_Private::EPropertyGenFlags::Bool | UE4CodeGen_Private::EPropertyGenFlags::NativeBool, RF_Public|RF_Transient|RF_MarkAsNative, 1, sizeof(bool), sizeof(TDSGameInstance_eventGetWeaponInfoByName_Parms), &Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_ReturnValue_SetBit, METADATA_PARAMS(nullptr, 0) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_NameWeapon,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_OutInfo,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::NewProp_ReturnValue,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::Function_MetaDataParams[] = {
		{ "ModuleRelativePath", "TDSGameInstance.h" },
	};
#endif
	const UE4CodeGen_Private::FFunctionParams Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::FuncParams = { (UObject*(*)())Z_Construct_UClass_UTDSGameInstance, nullptr, "GetWeaponInfoByName", nullptr, nullptr, sizeof(TDSGameInstance_eventGetWeaponInfoByName_Parms), Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::PropPointers, UE_ARRAY_COUNT(Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::PropPointers), RF_Public|RF_Transient|RF_MarkAsNative, (EFunctionFlags)0x04420401, 0, 0, METADATA_PARAMS(Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::Function_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::Function_MetaDataParams)) };
	UFunction* Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName()
	{
		static UFunction* ReturnFunction = nullptr;
		if (!ReturnFunction)
		{
			UE4CodeGen_Private::ConstructUFunction(ReturnFunction, Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName_Statics::FuncParams);
		}
		return ReturnFunction;
	}
	UClass* Z_Construct_UClass_UTDSGameInstance_NoRegister()
	{
		return UTDSGameInstance::StaticClass();
	}
	struct Z_Construct_UClass_UTDSGameInstance_Statics
	{
		static UObject* (*const DependentSingletons[])();
		static const FClassFunctionLinkInfo FuncInfo[];
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_WeaponInfoTable_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_WeaponInfoTable;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UTDSGameInstance_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UGameInstance,
		(UObject* (*)())Z_Construct_UPackage__Script_TDS,
	};
	const FClassFunctionLinkInfo Z_Construct_UClass_UTDSGameInstance_Statics::FuncInfo[] = {
		{ &Z_Construct_UFunction_UTDSGameInstance_GetWeaponInfoByName, "GetWeaponInfoByName" }, // 2525774848
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTDSGameInstance_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "TDSGameInstance.h" },
		{ "ModuleRelativePath", "TDSGameInstance.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UTDSGameInstance_Statics::NewProp_WeaponInfoTable_MetaData[] = {
		{ "Category", "WeaponSetting" },
		{ "Comment", "//table\n" },
		{ "ModuleRelativePath", "TDSGameInstance.h" },
		{ "ToolTip", "table" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_UTDSGameInstance_Statics::NewProp_WeaponInfoTable = { "WeaponInfoTable", nullptr, (EPropertyFlags)0x0010000000000005, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UTDSGameInstance, WeaponInfoTable), Z_Construct_UClass_UDataTable_NoRegister, METADATA_PARAMS(Z_Construct_UClass_UTDSGameInstance_Statics::NewProp_WeaponInfoTable_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UTDSGameInstance_Statics::NewProp_WeaponInfoTable_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UTDSGameInstance_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UTDSGameInstance_Statics::NewProp_WeaponInfoTable,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UTDSGameInstance_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UTDSGameInstance>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UTDSGameInstance_Statics::ClassParams = {
		&UTDSGameInstance::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		FuncInfo,
		Z_Construct_UClass_UTDSGameInstance_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		UE_ARRAY_COUNT(FuncInfo),
		UE_ARRAY_COUNT(Z_Construct_UClass_UTDSGameInstance_Statics::PropPointers),
		0,
		0x009000A8u,
		METADATA_PARAMS(Z_Construct_UClass_UTDSGameInstance_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UTDSGameInstance_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UTDSGameInstance()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UTDSGameInstance_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UTDSGameInstance, 3601569319);
	template<> TDS_API UClass* StaticClass<UTDSGameInstance>()
	{
		return UTDSGameInstance::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UTDSGameInstance(Z_Construct_UClass_UTDSGameInstance, &UTDSGameInstance::StaticClass, TEXT("/Script/TDS"), TEXT("UTDSGameInstance"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UTDSGameInstance);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
